
import numpy as np


class Rectangle():
    __version__ = "0.0.1"

    def __init__(self, p1, p2):

        self.x1 = p1[0]
        self.y1 = p1[1]
        self.x2 = p2[0]
        self.y2 = p2[1]
        self.a = self.area()
        self.p = None

    def compute_diagonal(self):
        return np.sqrt((self.x1 - self.x2)**2 + (self.y1 - self.y2)**2)

    def area(self):
        a, b = self.sides()
        return a*b

    def perimeter(self):
        p = 2.0*sum(self.sides())
        return p

    def sides(self):
        a = abs(self.x2 - self.x1)
        b = abs(self.y2 - self.y1)
        return a, b
