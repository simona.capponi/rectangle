numpy >= 1.5
pytest
pytest-cov
coverage
tox
flake8
