# %%
import unittest
from Rectangle.Rectangle import Rectangle
# from Rectangle import Rectangle
# %%


class RectangleTest(unittest.TestCase):

    def setUp(self):
        self.rectangle = RectangleTest.rectangle

    @classmethod
    def setUpClass(cls):
        super(RectangleTest, cls).setUpClass()
        cls.rectangle = Rectangle((0, 0), (1, 1))

    def test_diagonal(self):
        self.assertAlmostEqual(self.rectangle.compute_diagonal(), 1.4142135,
                               places=6, msg='wrong diagonal')

    def test_sides(self):
        self.assertListEqual(list(self.rectangle.sides()), [1.0, 1.0],
                             "sides incorrect")

    def test_area_p(self):
        self.assertEqual(self.rectangle.area(), 1.0,
                         "wrong area")
        self.assertEqual(self.rectangle.perimeter(), 4.0,
                         "wrong perimeter")


def suite():
    suite = unittest.TestSuite()
    suite.addTest(RectangleTest('test_diagonal'))
    suite.addTest(RectangleTest('test_sides'))
    suite.addTest(RectangleTest('test_area'))
    return suite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
